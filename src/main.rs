use gtk::prelude::*;


fn main() {
	let application = gtk::Application::new(Some("org.test"), Default::default());
	application.connect_activate(build_ui);
	application.run();
}

fn build_ui(application: &gtk::Application) {
		let window = gtk::ApplicationWindow::new(application);
		window.set_title(Some("test"));

		let buffer = gtk::TextBuffer::default();
		buffer.set_text("test");
		let tv = gtk::TextView::with_buffer(&buffer);

		let vbox = gtk::Box::new(gtk::Orientation::Vertical, 0);
		vbox.append(&tv);
		window.set_child(Some(&vbox));
		window.show();
}
